using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManejadorRecursos : MonoBehaviour
{

    /**Estos son los componentes que habilitaremos e inhabilitaremos
    *
    */
    [SerializeField]
    private TileReplacer tileReplacer; //Corresponde al componente TileReplacer
    [SerializeField]
    private DibujarCables dibujarCables; //Corresponde al componente DibujarCables

    /**
     * Bandera para activar y desactivar, no es suficiente con enabled por que dependemos del click de TilteReplacer
     */

    [SerializeField]
    public bool flagComponent = true;

    /**
     * Inicializamos dichos componentes
     */
    private void Start()
    {
        tileReplacer = GetComponent<TileReplacer>();
        dibujarCables = GetComponent<DibujarCables>();

        tileReplacer.enabled = true;
        dibujarCables.enabled = false;
    }

    /**
     * Aqu� hacemos el toggle de los componentes (habilitar o inhabilitar)
     */
    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyUp(KeyCode.Space))

        /*Si flagComponent true ->
         * TileReplacer -> True
         * DibujarCables -> false
         */
        //tileReplacer.enabled = !tileReplacer.enabled;
        //dibujarCables.enabled = !dibujarCables.enabled;
        if (flagComponent)
        {
            tileReplacer.enabled = true;
            dibujarCables.enabled = false;
            
        }
        else
        {
            tileReplacer.enabled = false;
            dibujarCables.enabled = true;
        }
    }
}
