using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIrecursos : MonoBehaviour
{

    /*
     * Objeto input manager para asignar el prefabInstanciar
     */
    [SerializeField]
    GameObject inputManager;

    /*
     * Prefab de referencia (recurso)
     */
    [SerializeField]
    GameObject prefab;



    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void OnMouseDown()
    {

        inputManager = GameObject.Find("InputManager");
        Debug.Log("prefab a asignar: " + this.prefab);
        
        //Para colocar el prefab en tileReplacer
        TileReplacer tilerep = inputManager.GetComponent<TileReplacer>();
        Debug.Log("prefabAInstanciar antes: "+tilerep.getPrefabAInstanciar());
        tilerep.setPrefabAInstanciar(this.prefab);
        Debug.Log("prefabAInstanciar despu�s: "+tilerep.getPrefabAInstanciar());

        //Para cambiar la bandera flagComponent a true / para colocar recursos al seleccionar uno
        if (tilerep.getPrefabAInstanciar())
        {
            ManejadorRecursos recursoManager = inputManager.GetComponent<ManejadorRecursos>();
            recursoManager.flagComponent = true;
        }

    }

}