using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SeleccionarNivel : MonoBehaviour
{
    /**
     * <summary>
     * Nombre de la escena a la que lleva
     * </summary>
     */
    [SerializeField]
    string escena;

    private void Start()
    {
        escena = this.name.ToString();
    }

    public void NivelSelect()
    {
        escena = this.name.ToString();
        Debug.Log("Cargando escena " + escena);
        SceneManager.LoadScene(escena);
    }

}
