using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    /**
     * <summary>
     * El canvas de men� principal
     * </summary>
     */
    [SerializeField]
    Canvas menuInicial;

    /**
     * <summary>
     * El canvas de elecci�n de nivel.
     * </summary>
     */
    [SerializeField]
    Canvas seleccionNivel;

    private void Start()
    {
        menuInicial.enabled = true;
        seleccionNivel.enabled = false;
    }

    /*
    * Funci�n para ir de selecci�n de nivel a men� inicial
    */
    public void MenuInicial()
    {
        seleccionNivel.enabled = false;
        menuInicial.enabled = true;

    }


    /*
     * Funci�n que lleva a la selecci�n de niveles
     */
    public void MenuSeleccionNivel()
    {
        menuInicial.enabled = false;
        seleccionNivel.enabled = true;

    }



}
