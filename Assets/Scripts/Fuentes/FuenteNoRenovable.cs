using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase que modela el comportamiento de las fuentes no renovables.
 * </summary>
 */
public class FuenteNoRenovable : Fuente {

    /**
     * Lo que cuesta esta fuente no renovable.
     */
    [SerializeField]
    int precio;

    /**
     * <summary>
     * Getter de precio.
     * </summary>
     * <returns>
     * this.precio
     * </returns>
     */
    public int getPrecio() {
        return this.precio;
    }

    /**
     * <summary>
     * Setter de energ�a
     * </summary>
     * <param name="precio">
     * El nuevo precio de esta fuente no renovable.
     * </param>
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /** 
    * <summary>
    * Constructor de FuenteNoRenovable. Las fuentes no renovables siempre
    * generan energ�a, sin necesidad de un generador.
    * </summary>
    */
    public FuenteNoRenovable() {
        setGenerando(true);
        setRenovable(false);
    }

    public override int activa(bool activa) {
        return activa ? (int) getEnerg�a() : 0;
    }

}
