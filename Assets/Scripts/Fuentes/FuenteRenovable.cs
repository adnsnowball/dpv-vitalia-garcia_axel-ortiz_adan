using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase que modela el comportamiento de las fuentes renovables.
 * </summary>
 */
public class FuenteRenovable : Fuente {

    /** 
    * <summary>
    * Constructor de FuenteRenovable. Las fuentes renovables
    * no generan energ�a a menos que se les ponga el generador
    * correcto.
    * </summary>
    */
    public FuenteRenovable() {
        setGenerando(false);
        setRenovable(true);
    }

    public override int activa(bool activa) {
        setGenerando(activa);
        return activa ? (int) getEnerg�a() : 0;
    }

}
