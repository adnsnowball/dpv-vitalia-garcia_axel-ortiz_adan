using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase abstracta que define el comportamiento de las fuentes.
 * Esta se implementa en FuentesRenovables y FuentesNoRenovables.
 * </summary>
 */
public abstract class Fuente : MonoBehaviour {
    /**
     * <summary>
     * La cantidad de energ�a que genera esta fuente.
     * </summary>
     */
    [SerializeField]
    Energ�a energ�a;

    /**
     * <summary>
     * Booleano que indica si esta fuente es renovable o no.
     * </summary>
     */
    private bool renovable;

    /**
     * <summary>
     * Booleano que indica si esta fuente est� generando energ�a o no.
     * </summary>
     */
    [SerializeField]
    bool generando;

    /**
     * <summary>
     * Getter de energ�a.
     * </summary>
     * <returns>
     * this.energ�a
     * </returns>
     */
    public Energ�a getEnerg�a() {
        return this.energ�a;
    }

    /**
     * <summary>
     * Getter de renovable.
     * </summary>
     * <returns>
     * this.renovable
     * </returns>
     */
    public bool getRenovable()
    {
        return this.renovable;
    }

    /**
     * <summary>
     * Getter de generando.
     * </summary>
     * <returns>
     * this.generando
     * </returns>
     */
    public bool getGenerando()
    {
        return this.generando;
    }

    /**
     * <summary>
     * Setter de energ�a
     * </summary>
     * <param name="energ�a">
     * La nueva energ�a de esta fuente.
     * </param>
     */
    public void setEnerg�a(Energ�a energ�a)
    {
        this.energ�a = energ�a;
    }

    /**
     * <summary>
     * Setter de renovable.
     * </summary>
     * <param name="renovable">
     * El nuevo estado de renovable de esta fuente.
     * </param>
     */
    public void setRenovable(bool renovable) {
        this.renovable = renovable;
    }

    /**
     * <summary>
     * Setter de generando.
     * </summary>
     * <param name="generando">
     * El nuevo estado de generando de esta fuente.
     * </param>
     */
    public void setGenerando(bool generando)
    {
        this.generando = generando;
    }

    /**
     * <summary>
     * M�todo abstracto que modela el activar esta fuente para que genere energ�a.
     * </summary>
     * <param name="activa">
     * Booleano que indica si esta fuente va a generar energ�a o no.
     * </param>
     */
    public abstract int activa(bool activa);

}
