/**
 * Enum que guarda los diferentes niveles de energía.S
 */
public enum Energía {

    Baja = 6,
    MediaBaja = 12,
    MediaAlta = 18,
    Alta = 24

}
