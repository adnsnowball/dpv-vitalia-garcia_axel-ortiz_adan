using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * <summary>
 * Clase que modela el comportamiento de los generadores.
 * </summary>
 */
public class Generador : Conectable, Emisor {

    [SerializeField]
    Energía energía;

    /**
     * Precio del generador. 
     */
    [SerializeField]
    int precioGenerador;

    /**
     * La energía que está generando el generador actualmente.
     */
    [SerializeField]
    int cantidadEnergía;

    /**
     * bool que indica si este generador está generadno energía.
     */
    [SerializeField]
    bool generando;

    /**
     * El Animator de este Generador.
     */
    Animator animator;

    public int getCantidadEnergía() {
        return this.cantidadEnergía;
    }

    public int getPrecioGenerador() {
        return this.precioGenerador;
    }

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        animator.SetBool("Generando", generando);
    }

    public void generar(bool activa, Fuente fuente) {
        Energía energíaFuente = fuente.getEnergía();
        this.cantidadEnergía = fuente.activa(activa);
        this.generando = fuente.getGenerando();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Fuente fuente = collision.GetComponent<Fuente>();
        if (fuente) {
            bool mismaEnergía = false;
            if (fuente.getEnergía().Equals(energía)) {
                mismaEnergía = true;
            }
            generar(mismaEnergía, fuente);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        Fuente fuente = collision.GetComponent<Fuente>();
        if (fuente) {
            generar(false, fuente);
        }
    }

    public int energíaPorConexión() {
        int numSucesores = this.getSucesores().Count;
        int energíaPorConexion = cantidadEnergía;
        if (numSucesores > 0) {
            energíaPorConexion = cantidadEnergía / numSucesores;
        }
        return energíaPorConexion;
    }

    public void repartirEnergía() {
        foreach (Conectable sucesor in this.getSucesores()) {
            Receptor receptor = sucesor as Receptor;
            if (receptor != null) {
                receptor.revisarEnergía();
            }
        }
    } 
}
