using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
* Script de comportamiento que se encarga de instanciar
* GameObjects sobre el mundo siguiendo las coordenadas
* de un tilemap.
*/
public class TileReplacer : MonoBehaviour {
    
    /**
     * El objeto que queremos instanciar.
     */
    [SerializeField]
    private GameObject prefabAInstanciar;

    /**
     * El grid del cual queremos las coordenadas de 
     * sus tiles.
     */
    [SerializeField]
    private Grid targetGrid;

    /**
     * La c�mara principal.
     */
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start() {
        this.mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Si dio click para colocar el prefab
        if (Input.GetMouseButtonDown(0) && prefabAInstanciar) {
            Vector3 mouseWorldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int coordinate = targetGrid.WorldToCell(mouseWorldPos);
            Vector3 posAbs = targetGrid.GetCellCenterWorld(coordinate);
            posAbs.z = 0;

            if (!instanceHitsSomething(posAbs)) {
                GameObject newObject = Instantiate(prefabAInstanciar, posAbs, Quaternion.identity);
                newObject.transform.localScale = targetGrid.transform.localScale;
            }

            this.GetComponent<ManejadorRecursos>().flagComponent = false;
            //Quitamos el prefab
            this.prefabAInstanciar = null;
        }


    }


    /**
     * <summary>
     * Setter de prefabAInstanciar.
     * </summary>
     * <param name="prefabAInstanciar">
     * El nuevo recurso para colocar
     * </param>
     */
    public void setPrefabAInstanciar(GameObject prefab) {
        this.prefabAInstanciar = prefab;
    }

    /**
     * <summary>
     * Getter de prefabAInstanciar.
     * </summary>
     * <param name="prefabAInstanciar">
     * El recurso para colocar
     * </param>
     */
    public GameObject getPrefabAInstanciar() {
        return prefabAInstanciar;
    }

    /**
     * <summary>
     * Revisa si, dadas unas coordenadas, existe un objeto con un collider que tenga el tipo gen�rico
     * como uno de sus componentes.
     * </summary>
     * <param name="coords">
     * Las coordenadas desde las cuales se quiere hacer el Raycast. Como es un Raycast2D, se utilizan
     * solo el componente x y el componente y.
     * </param>
     */
    private T checkCoordsOnInstance<T>(Vector3 coords) {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(coords.x, coords.y), Vector2.zero, 1);
        if (hit) {
            Collider2D collision = hit.transform.GetComponent<Collider2D>();
            if (collision) {
                T instancia = collision.gameObject.GetComponent<T>();
                if (instancia != null) {
                    // Debug.Log("Hola. Aqu� hay un objeto con un componente de tipo " + instancia.ToString());
                    return instancia;
                }
            }
        }

        return default;
    }

    /**
     * Revisa si los las coordenadas de los puntos intermedios entre las posiciones inicial y la final
     * no contienen un obstaculo. Devuelven un booleano dependiendo si existe o no un obst�culo.
     */
    private bool instanceHitsSomething(Vector3 coords) {
        bool status = false;
        status = checkCoordsOnInstance<Obstaculo>(coords) || status;
        status = checkCoordsOnInstance<Cable>(coords) || status;
        status = checkCoordsOnInstance<Conectable>(coords) || status;
        return status;
    }
}
