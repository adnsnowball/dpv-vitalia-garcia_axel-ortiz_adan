using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/**
 * <summary>
 * Script que maneja el flujo entre escenas y los recursos del nivel.
 * </summary>
 */
public class LevelManager : MonoBehaviour {

    /**
     * <summary>
     * Entero que mide el presupuesto que tiene cada nivel, es decir, 
     * el m�ximo de dinero que el usuario se puede gastar en recursos
     * antes de quedar endeudado.
     * </summary>
     */
    [SerializeField]
    int presupuesto;

    /**
     * <summary>
     * El dinero que se ha gastado el usuario hasta el momento.
     * </summary>
     */
    [SerializeField]
    int dineroGastado;

    /**
     * <summary>
     * Bandera booleana que indica si el nivel ya est� completado; es
     * decir, si todas las Ciudades en el nivel ya han sido energizadas.
     * </summary>
     */
    [SerializeField]
    bool nivelCompletado;

    /**
     * <summary>
     * La lista de referencias a los Conectables en la escena en todo
     * momento. Solo pueden ser Generadores o Nodos.
     * </summary>
     */
    [SerializeField]
    Conectable[] conectables;

    /**
     * <summary>
     * La lista de referencias a los puntos extra de este nivel. Cada nivel tiene 3 puntos extra.
     * </summary>
     */
    [SerializeField]
    PuntoExtra[] puntosExtra;

    /**
     * <summary>
     * La cantidad de puntos extra energizados.
     * </summary>
     */
    [SerializeField]
    int puntosExtraEnergizados;

    /**
     * <summary>
     * El canvas que surge al terminar el nivel.
     * </summary>
     */
    [SerializeField]
    Canvas nivelCompletadoCanvas;


    /**
     * <summary>
     * El canvas que surge al pausar el nivel.
     * </summary>
     */
    [SerializeField]
    Canvas pausaCanvas;

    /**
     * <summary>
     * El canvas de recursos y botones de nivel en funcionamiento.
     * </summary>
     */
    [SerializeField]
    Canvas nivelCanvas;

    /*
     * Texto para el puntaje de costos
     */
    [SerializeField]
    Text textoCostoActual;

    /*
     * Texto para el n�mero de casillas de puntos extra energizadas.
     */
    [SerializeField]
    Text textopuntosExtraEnergizados;

    /*
     * Nivel actual
     */
    [SerializeField]
    Text textoNivelActual;

    /*
     * Variable que indica en qu� nivel / escena estamos.
     * Su �nica funci�n es permitir continuar en el nivel en el que se qued� el jugador.
     * Se actualiza conforme se actualice la escena
     */
    [SerializeField]
    string nivelAContinuar;


    /*
     * Variable que nos permite saber si el usuario ya pas� las gu�as y el flujo entre estas y niveles
     */
    [SerializeField]
    int tutorial = 0;

    // Start is called before the first frame update
    void Start() {
        dineroGastado = 0;
        nivelCompletadoCanvas.enabled = false;
        //pausaCanvas = GameObject.Find("PausaCanvas");
        pausaCanvas.enabled = false;
        nivelCanvas.enabled = true;
        puntosExtra = GameObject.FindObjectsOfType<PuntoExtra>();

        //Aqu� guardamos el nivel Actual, es decir, en qu� nivel nos encontramos
        Scene sceneActual = SceneManager.GetActiveScene();
        nivelAContinuar = sceneActual.name;
        //S�lamente guarda lo que es niveles, no si es men� o selecci�n niveles, o configuraci�n, etc.
        if(nivelAContinuar != "Menu")
        {
            Debug.Log(nivelAContinuar);
            PlayerPrefs.SetString("nivelAContinuar", nivelAContinuar);
            PlayerPrefs.Save();
        }

        //Actualizamos el texto de nivel actual
        if(sceneActual.name != "Menu" && sceneActual.name != "Gu�a1" && sceneActual.name != "Gu�a2" && sceneActual.name != "Gu�a3")
        textoNivelActual.text = sceneActual.name.ToString();
        textoNivelActual.text = textoNivelActual.text.Substring(5);
        textoNivelActual.text = ("N.") + textoNivelActual.text;
    }

    // Update is called once per frame
    void Update() {
        conectables = GameObject.FindObjectsOfType<Conectable>();
        int dineroEnReferencias = 0;
        bool todasCiudadesEnergizadas = true;
        int puntosExtraEnergizadosActual = 0;

        foreach (Conectable conectable in conectables) {
            Generador generador = conectable.GetComponent<Generador>();
            Nodo nodo = conectable.GetComponent<Nodo>();
            Ciudad ciudad = conectable.GetComponent<Ciudad>();
            if (generador) {
                dineroEnReferencias += generador.getPrecioGenerador();
            } else if (nodo) {
                dineroEnReferencias += nodo.getPrecioNodo();
            } else if (ciudad) {
                todasCiudadesEnergizadas = todasCiudadesEnergizadas && ciudad.isEnergizada();
            }
        }

        foreach (PuntoExtra puntoExtra in puntosExtra) {
            if (puntoExtra.isEnergizado()) {
                puntosExtraEnergizadosActual += 1;
            }
        }

        dineroGastado = dineroEnReferencias;
        textoCostoActual.text = (presupuesto-dineroGastado).ToString();
        textoCostoActual.color = Color.white;
        nivelCompletado = todasCiudadesEnergizadas;
        puntosExtraEnergizados = puntosExtraEnergizadosActual;


        if (dineroGastado > presupuesto) {
            textoCostoActual.color = Color.red;
        }

        if (nivelCompletado) {
            nivelCompletadoCanvas.enabled = true;
        }

        //Actualizamos los puntos energizados y la imagen
        CheckPuntosExtraEnergizados(puntosExtraEnergizados);
    }

    public void SiguienteNivel() {
        //Llamamos a la funci�n que checa si viene gu�a o no
        string guia = CheckGuia(SceneManager.GetActiveScene().buildIndex + 1);
        if(guia == "Gu�a0")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(guia);
        }
    }

    public void ReiniciarNivel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    /*
     * Funci�n para pausar nivel
     */
    public void PausarNivel()
    {
        Debug.Log("PAUSSAAAA");
        //Time.timeScale = 0;
        pausaCanvas.enabled = true;
        nivelCanvas.enabled = false;
    }


    /*
     * Funci�n para despausar nivel
     */
    public void ContinuarNivel()
    {
        Debug.Log("CONTINUAAAr");
        //Time.timeScale = 1;
        pausaCanvas.enabled = false;
        nivelCanvas.enabled = true;
    }

    /*
     * Bot�n para ir al nivel anterior
     */
    public void NivelAnterior()
    {
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name != "Nivel1")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }

    public void MenuInicial()
    {
        //Debug.Log("Yendo al men� inicial");
        SceneManager.LoadScene("Menu");
    }


    /*
     * Funci�n para empezar desde el nivel 1
     */
    public void IniciarNiveles()
    {
        //Llamamos a la funci�n que checa si viene gu�a o no
        string guia = CheckGuia(1);
        if (guia == "Gu�a0")
        {
            SceneManager.LoadScene("Nivel1");
        }
        else
        {
            SceneManager.LoadScene(guia);
        }
    }

    /*
     * Continuar nivel desde la pantalla del men� principal
     */
    public void MenuContinuarNivel()
    {
        nivelAContinuar = PlayerPrefs.GetString("nivelAContinuar");
        //Debug.Log("Escena a continuar: " + nivelAContinuar);
        SceneManager.LoadScene(nivelAContinuar);
    }


    /*
     * Con esta funci�n actualizamos los puntos energizados y la imagen correspondiente
     */
    public void CheckPuntosExtraEnergizados(int puntos)
    {
        textopuntosExtraEnergizados.text = puntosExtraEnergizados.ToString();
        var EnergizadosNivel0 = GameObject.Find("EnergizadosNivel0");
        var EnergizadosNivel1 = GameObject.Find("EnergizadosNivel1");
        var EnergizadosNivel2 = GameObject.Find("EnergizadosNivel2");
        var EnergizadosNivel3 = GameObject.Find("EnergizadosNivel3");

        //EnergizadosNivel0.SetActive(false);
        //EnergizadosNivel1.SetActive(false);
        //EnergizadosNivel2.SetActive(false);
        //EnergizadosNivel3.SetActive(false);

        if (puntos<= 0)
        {
            //EnergizadosNivel0.SetActive(true);
            //Debug.Log("EnergizadosNivel -> 0");
        }
        else if(puntos == 1)
        {
            //Debug.Log("EnergizadosNivel -> 1");
        }
        else if (puntos == 2)
        {
            //Debug.Log("EnergizadosNivel -> 2");
        }
        else if(puntos == 3)
        {
            //EnergizadosNivel3.SetActive(true);
            //Debug.Log("EnergizadosNivel -> 3");
        }

        //Aqu� debemos obtener dicha imagen, ejemplo: "EnergizadosNivel1"
    }



    /*
     * Funciones de transici�n de gu�as niveles 
     */

    public void GuiaCompletada()
    {
        int tutorial = PlayerPrefs.GetInt("tutorial");
        //Dependiendo la gu�a / tutorial hecho, es al nivel que lleva
        tutorial += 1;
        PlayerPrefs.SetInt("tutorial", tutorial);
        SceneManager.LoadScene("Nivel"+tutorial);
    }

    public string CheckGuia(int siguienteNivel)
    {
        string guia = "Gu�a0";
        //Obtenemos la varibale que nos dice qu� gu�as o tutoriales ya fueron vistos
        tutorial = PlayerPrefs.GetInt("tutorial");
        Debug.Log("Valor de tutorial actual: " + tutorial);
        Debug.Log("Valor de sig. nivel: " + siguienteNivel);


        if ( siguienteNivel == 1 && tutorial == 0)
        {
            guia = "Gu�a1";
        }
        else if (siguienteNivel == 2 && tutorial == 1)
        {
            guia = "Gu�a2";
        }
        else if (siguienteNivel == 3 && tutorial == 2)
        {
            guia = "Gu�a3";
        }

        return guia;

    }



    /*
     * Funci�n que reinicia stats
     * Tutoriales hechos
     * Puntajes etc.
     */
    public void ReiniciarStats()
    {
        int tutoActual = PlayerPrefs.GetInt("tutorial");
        Debug.Log("�ltimo tuto completado: " + tutoActual);
        PlayerPrefs.SetInt("tutorial", 0);
        tutoActual = PlayerPrefs.GetInt("tutorial");
        Debug.Log("Valor actual de tutoriales completados: " + tutoActual);
    }


    public void Salir()
    {
        Application.Quit();
    }

}
