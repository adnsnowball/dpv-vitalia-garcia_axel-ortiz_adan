using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/**
 * <summary>
 * Script que se encarga de controlar el dibujo de cables, creando
 * instancias el prefab en cada celda entre las celdas inicial y final.
 * </summary>
 */
public class DibujarCables : MonoBehaviour {
    /**
     * <summary>
     * El prefab (cable) a instanciar.
     * </summary>
     */
    [SerializeField]
    GameObject cable;

    /**
     * <summary>
     * El grid del cual queremos las coordenadas de 
     * sus tiles.
     * </summary>
     */
    [SerializeField]
    private Grid targetGrid;

    /**
     * <summary>
     * La c�mara principal.
     * </summary>
     */
    private Camera mainCamera;

    /**
     * <summary>
     * Vector que guarda las coordenadas globales desde donde inicia
     * el cable a dibujar. Este se actualiza en cada click.
     * </summary>
     */
    private Vector3 startPoint;

    /**
     * <summary>
     * Vector que guarda las coordenadas globales hasta donde termina
     * el cable a dibujar. Este se actualiza en cada click.
     * </summary>
     */
    private Vector3 endPoint;

    /**
     * <summary>
     * Vector de coordenadas enteras que guarda la celda desde donde
     * inica el cable a dibujar. Este se actualiza en cada click.
     * </summary>
     */
    private Vector3Int startPos;

    /**
     * <summary>
     * Vector de coordenadas enteras que guarda la celda hasta donde
     * termina el cablea dibujar. Este se actualiza en cada click.
     * </summary>
     */
    private Vector3Int endPos;

    /**
     * <summary>
     * Lista que guarda los puntos intermedios entre las posiciones
     * inicial y final. Se actualiza en cada click.
     * </summary>
     */
    private List<Vector3> midpoints;

    /**
     * Bandera booleana que le dice al script si se puede iniciar el 
     * dibujo del cable.
     */
    private bool puedeDibujarInicio;

    /**
     * Bandera booleana que le dice al script si se puede terminar el 
     * dibujo del cable.
     */
    private bool puedeDibujarFinal;

    /**
     * El Conectable desde donde se inicia el cable.
     */
    private Conectable predecesor;

    /**
     * El Conectable donde termina el cable.
     */
    private Conectable sucesor;

    /**
     * LineRenderer para retroalimentaci�n visual del dibujo del cable
     */
    [SerializeField]
    LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start() {
        this.mainCamera = Camera.main;
        midpoints = new List<Vector3>();
        puedeDibujarInicio = false;
        puedeDibujarFinal = false;
        sucesor = null;
        predecesor = null;
        lineRenderer.widthMultiplier = targetGrid.transform.localScale.y;
    }

    // Update is called once per frame
    void Update() {
        // Al iniciar una acci�n click-drag-drop, se guarda la posici�n inicial
        if (Input.GetMouseButtonDown(0)) {
            Vector3 mouseWorldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            startPos = targetGrid.WorldToCell(mouseWorldPos);
            startPoint = targetGrid.GetCellCenterWorld(startPos);
            startPoint.z = 0;
            // Revisamos que el click inicial sea sobre un objeto de tipo Conectable.
            Conectable conectable = checkCoordsOnInstance<Conectable>(startPoint);
            if (conectable) {
                // Revisamos que este Conectable permita m�s conexiones.
                if (conectable.permiteConectar()) {
                    // Referenciamos este conectable como la salida
                    this.predecesor = conectable;
                    // puedeDibujarInicio ser� true solo si el Conectable es de tipo
                    // Nodo o Generador.
                    Nodo nodo = checkCoordsOnInstance<Nodo>(startPoint);
                    Generador generador = checkCoordsOnInstance<Generador>(startPoint);
                    puedeDibujarInicio = nodo || generador;
                    // Iniciamos el LineRenderer
                    Vector3 startPointLineRenderer = new Vector3(startPoint.x, startPoint.y, -1);
                    lineRenderer.enabled = true;
                    lineRenderer.positionCount = 2;
                    lineRenderer.SetPosition(0, startPointLineRenderer);
                    lineRenderer.useWorldSpace = true;
                }
            }
        }

        // La posici�n final se actualiza constantemente mientras se mantenga apretado
        // el bot�n del mouse.
        if (Input.GetMouseButton(0)) {
            Vector3 mouseWorldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            endPos = targetGrid.WorldToCell(mouseWorldPos);
            endPoint = targetGrid.GetCellCenterWorld(endPos);
            endPoint.z = 0;
            Vector3 endPointLineRenderer = new Vector3(endPoint.x, endPoint.y, -1);
            lineRenderer.SetPosition(1, endPointLineRenderer);
        }

        // Se utiliza la posici�n inicial y la �ltima posici�n final para calcular las celdas
        // intermedias y dibujar el cable, de permitirse.
        if (Input.GetMouseButtonUp(0)) {
            // Revisamos que el click inicial sea sobre un objeto de tipo Conectable.
            Conectable conectable = checkCoordsOnInstance<Conectable>(endPoint);
            if (conectable) {
                // Revisamos que este Conectable permita m�s conexiones.
                if (conectable.permiteConectar()) {
                    // Referenciamos este conectable como la entrada
                    this.sucesor = conectable;
                    // puedeDibujarFinal ser� true solo si el Conectable es de tipo
                    // Nodo o Ciudad.
                    Nodo nodo = checkCoordsOnInstance<Nodo>(endPoint);
                    Ciudad ciudad = checkCoordsOnInstance<Ciudad>(endPoint);
                    puedeDibujarFinal = nodo || ciudad;
                }
            }
            // Solo en caso de que las celdas inicial y final sean correctas procedemos a dibujar.
            if (puedeDibujarInicio && puedeDibujarFinal) {
                CalculateMidPoints();
                // Revisamos que no haya obst�culos atravesando el cable.
                if (!midpointsHitObstacle()) {
                    drawLine();
                }
            }
            // Reiniciamos las variables para el siguiente trazo de cable.
            midpoints.Clear();
            puedeDibujarInicio = false;
            puedeDibujarFinal = false;
            lineRenderer.enabled = false;
        }
    }

    /**
     * <summary>
     * Dibuja el cable entre los puntos inicial y final del trazo, pero solo en caso de que est� en
     * un �ngulo permitido. Los �ngulos permitidos son los congruentes con 30 m�dulo 60 (en grados).
     * </summary>
     */
    void drawLine() {
        double angle = Math.Round(CalculateAngle(Vector3.right, endPoint - startPoint));
        if ((angle - 30.0f) % 60 == 0) {
            Vector3 midpoint = (startPoint + endPoint)/2;
            Cable nuevoCable = Instantiate(cable, midpoint, Quaternion.AngleAxis((float)angle, Vector3.forward)).GetComponent<Cable>();
            nuevoCable.transform.localScale = new Vector3(midpoints.Count * targetGrid.transform.localScale.x, targetGrid.transform.localScale.y, targetGrid.transform.localScale.z);
            nuevoCable.setPredecesor(predecesor);
            nuevoCable.setSucesor(sucesor);
            nuevoCable.conectar();
            Debug.Log("Se han conectado estos dos conectables");
        }

    }

    /**
     * <summary>
     * Calcula el �ngulo que forma el arco comprendido entre dos vectores con respecto al eje x. El
     * arco se mide en sentido antihorario, en 360�.
     * </summary>
     * <param name="from">
     * Vector que conforma el �ngulo desde el cual se mide.
     * </param>
     * * <param name="to">
     * Vector que conforma el �ngulo hasta el cual se mide.
     * </param>
     * <returns>
     * Un flotante entre 0.0 y 360.0, representando el �ngulo en grados.
     * </returns>
     */
    static float CalculateAngle(Vector3 from, Vector3 to) {
        float angle = Vector3.SignedAngle(from, to, Vector3.forward);
        return (angle < 0) ? angle = 360f - (angle * -1) : angle;
    }

    /**
     * <summary>
     * Convierte un vector en coordendas (x, y, z) de tipo offset que utilizan las celdas hexagonales
     * de Unity en un vector de coordenadas (q, r, s) de tipo c�bicas.
     * <summary>
     */
    Vector3Int unityCellToCube(Vector3Int cell) {
        int xCell = cell.y;
        int yCell = cell.x;
        int x = yCell - (xCell - (xCell & 1)) / 2;
        int z = xCell;
        return new Vector3Int(x, -x-z, z);
    }

    /**
     * <summary>
     * Convierte un vector en coordenadas (q, r, s) de tipo c�bicas en un vecto de coordenadas (x, y, z)
     * de tipo offset que utilizan las celdas hexagonales de Unity.
     * <summary>
     */
    private Vector3Int cubeToUnityCell(Vector3Int cube) {
        int x = cube.x;
        int z = cube.z;
        int col = x + (z - (z & 1)) / 2;
        int row = z;
        return new Vector3Int(col, row, 0);
    }

    /**
     * <summary>
     * Calcula los puntos intermedios entre las celdas inicial y final, dependiendo del caso,
     * y los agrega a la lista de midpoints.
     * <summary>
     */
    public void CalculateMidPoints() {
        Vector3Int cubeStartPos = unityCellToCube(startPos);
        Vector3Int cubeEndPos = unityCellToCube(endPos);

        if (cubeStartPos.z == cubeEndPos.z) {
            int minCoordX = Math.Min(cubeStartPos.x, cubeEndPos.x);
            int maxCoordX = Math.Max(cubeStartPos.x, cubeEndPos.x);
            int maxCoordY = Math.Max(cubeStartPos.y, cubeEndPos.y);

            for (int i = 1; i < Math.Abs(maxCoordX - minCoordX); i++) {
                Vector3Int cubeCurrentMidPos = new Vector3Int(minCoordX + i, maxCoordY - i, cubeStartPos.z);
                Vector3Int currentMidPos = cubeToUnityCell(cubeCurrentMidPos);
                midpoints.Add(targetGrid.GetCellCenterWorld(currentMidPos));
            }
        } else if(cubeStartPos.y == cubeEndPos.y) {
            int minCoordX = Math.Min(cubeStartPos.x, cubeEndPos.x);
            int maxCoordX = Math.Max(cubeStartPos.x, cubeEndPos.x);
            int maxCoordZ = Math.Max(cubeStartPos.z, cubeEndPos.z);

            for (int i = 1; i < Math.Abs(maxCoordX - minCoordX); i++) {
                Vector3Int cubeCurrentMidPos = new Vector3Int(minCoordX + i, cubeStartPos.y, maxCoordZ - i);
                Vector3Int currentMidPos = cubeToUnityCell(cubeCurrentMidPos);
                midpoints.Add(targetGrid.GetCellCenterWorld(currentMidPos));
            }
        }
        else if(cubeStartPos.x == cubeEndPos.x) {
            int minCoordY = Math.Min(cubeStartPos.y, cubeEndPos.y);
            int maxCoordY = Math.Max(cubeStartPos.y, cubeEndPos.y);
            int maxCoordZ = Math.Max(cubeStartPos.z, cubeEndPos.z);

            for (int i = 1; i < Math.Abs(maxCoordY - minCoordY); i++) {
                Vector3Int cubeCurrentMidPos = new Vector3Int(cubeStartPos.x, minCoordY + i, maxCoordZ - i);
                Vector3Int currentMidPos = cubeToUnityCell(cubeCurrentMidPos);
                midpoints.Add(targetGrid.GetCellCenterWorld(currentMidPos));
            }
        }
    }

    /**
     * <summary>
     * Revisa si, dadas unas coordenadas, existe un objeto con un collider que tenga el tipo gen�rico
     * como uno de sus componentes.
     * </summary>
     * <param name="coords">
     * Las coordenadas desde las cuales se quiere hacer el Raycast. Como es un Raycast2D, se utilizan
     * solo el componente x y el componente y.
     * </param>
     */
    public T checkCoordsOnInstance<T>(Vector3 coords) {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(coords.x, coords.y), Vector2.zero, 1);
        if(hit) {
            Collider2D collision = hit.transform.GetComponent<Collider2D>();
            if (collision) {
                T instancia = collision.gameObject.GetComponent<T>();
                if (instancia != null) {
                    // Debug.Log("Hola. Aqu� hay un objeto con un componente de tipo " + instancia.ToString());
                    return instancia;
                }
            }
        }

        return default;
    }

    /**
     * Revisa si los las coordenadas de los puntos intermedios entre las posiciones inicial y la final
     * no contienen un obstaculo. Devuelven un booleano dependiendo si existe o no un obst�culo.
     */
    private bool midpointsHitObstacle() {
        bool status = false;
        foreach (Vector3 coords in midpoints) {
            status = checkCoordsOnInstance<Obstaculo>(coords) || status;
            status = checkCoordsOnInstance<Cable>(coords) || status;
            status = checkCoordsOnInstance<Conectable>(coords) || status;
            status = checkCoordsOnInstance<Fuente>(coords) || status;
        }
        return status;
    }
    
}
