using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* <summary>
* Script que modela el comportamiento de los colliders externos de los
* cables; es decir, la entrada y la salida.
* <summary>
*/
public class CollidersCable : MonoBehaviour {

    /**
     * <summary>
     * El cable de quien es hijo este collider.
     * <summary>
     */
    //private Cable cable;

    //private void Start() {
    //    this.cable = transform.GetComponentInParent<Cable>();
    //}

    //private void OnTriggerEnter2D(Collider2D collision) {
    //    Conectable conectable = collision.GetComponent<Conectable>();
    //    Cable cableContiguo = collision.GetComponent<Cable>();
    //    if (conectable) {
    //        System.Type tipo = conectable.GetType();
    //        if (tipo == typeof(Generador)) {
    //            Generador generador = (Generador)conectable;
    //            return;
    //        } else if (tipo == typeof(Nodo)) {
    //            Debug.Log("Hola, soy un nodo");
    //            return;
    //        } else if (tipo == typeof(Ciudad)) {
    //            Ciudad ciudad = (Ciudad)conectable;
    //            // Revisar que esta ciudad pueda conectarse a otro cable m�s.
    //            // En caso true, dividir la energ�a entre los cables.
    //            // En caso false, no conectar este cable al generador.
    //            return;
    //        } else {
    //            Debug.Log("Hola, no s� qu� soy");
    //            return;
    //        }
    //    } else if (cableContiguo) {
    //        cable.conectar(cableContiguo);
    //    }
    //}

    //private void OnTriggerExit2D(Collider2D collision) {
    //    Conectable conectable = collision.GetComponent<Conectable>();
    //    Cable cableContiguo = collision.GetComponent<Cable>();
    //    if (conectable) {
    //        System.Type tipo = conectable.GetType();
    //        if (tipo == typeof(Generador)) {
    //            Generador generador = (Generador)conectable;
    //            return;
    //        }
    //        else if (tipo == typeof(Nodo)) {
    //            Debug.Log("Hola, soy un nodo");
    //            return;
    //        }
    //        else if (tipo == typeof(Ciudad)) {
    //            Ciudad ciudad = (Ciudad)conectable;
    //            return;
    //        }
    //        else {
    //            Debug.Log("Hola, no s� qu� soy");
    //            return;
    //        }
    //    } else if (cableContiguo) {
    //        cable.desconectar(cableContiguo);
    //    }
    //}

}
