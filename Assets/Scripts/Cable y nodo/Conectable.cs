using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase abstracta que modela el compoertamiento de las celdas conectables.
 * Una celda es "conectable" es toda celda que puede ser conectada por
 * Cables; es decir, Nodos, Ciudades y Generadores.
 */
public abstract class Conectable : MonoBehaviour {

    /**
     * La cantidad de conexiones disponibles para los Conectables.
     */
    [SerializeField]
    int cantConexiones;

    /**
     * La lista de Conectables conectados de este Conectable.
     */
    // [SerializeField]
    // List<Conectable> vecinos

    /**
     * La lista de Conectables predecesores conectados de este Conectable.
     */
    [SerializeField]
    List<Conectable> predecesores;

    /**
     * La lista de Conectables sucesores conectados de este Conectable.
     */
    [SerializeField]
    List<Conectable> sucesores;

    /**
     * La lista de Cables de este conectable
     */
    [SerializeField]
    List<Cable> cablesVecinos;

    /**
     * Atributos para leer el doble click.
     */
    //public float DoubleClickTimeout = 0.2f;
    //private bool clickEnable = true;
    //private bool doubleClick = false;

    /**
     * Constructor de este Conectable.
     */
    public Conectable()
    {
        predecesores = new List<Conectable>();
        sucesores = new List<Conectable>();
        cablesVecinos = new List<Cable>();
    }

    /**
     * <summary>
     * Getter de cantConexiones.
     * </summary>
     * <returns>
     * this.cantConexiones
     * </returns>
     */
    public int getCantConexiones()
    {
        return this.cantConexiones;
    }

    public List<Conectable> getPredecesores()
    {
        return this.predecesores;
    }

    public List<Conectable> getSucesores() {
        return this.sucesores;
    }

    /**
     * <summary>
     * Setter de cantConexiones.
     * </summary>
     * <param name="cantConexiones">
     * La nueva cantidad de conexiones de este Conectable.
     * </param>
     */
    public void setEnergía(int cantConexiones)
    {
        this.cantConexiones = cantConexiones;
    }

    public List<Cable> getCablesVecinos() {
        return this.cablesVecinos;
    }

    public bool permiteConectar() {
        int cantPredecesores = this.predecesores.Count;
        int cantSucesores = this.sucesores.Count;
        if ((cantPredecesores + cantSucesores) < this.getCantConexiones()) {
            return true;
        } else {
            return false;
        }
    }

    private Cable getCableToVecino(Conectable vecino) {
        List<Cable> cablesDeVecino = vecino.getCablesVecinos();
        foreach(Cable cableDeVecino in cablesDeVecino) {
            foreach(Cable cableVecino in this.cablesVecinos) {
                if (cableDeVecino.Equals(cableVecino)) {
                    return cableDeVecino;
                }
            }
        }
        return null;
    }

    private void OnMouseOver() {
        if (!gameObject.GetComponent<Ciudad>()) {
            if (Input.GetMouseButtonDown(1)) {
                Debug.Log("Click derecho");
                // Revisamos los predecesores de este nodo, desconectando su cable.
                // Recordamos que solo los Emisores pueden ser predecesores, por lo que
                // hacemos que vuelva a repartir su energía entre sus Receptores.
                foreach (Conectable predecesor in this.predecesores) {
                    Cable cableToPredecesor = getCableToVecino(predecesor);
                    predecesor.getSucesores().Remove(this);
                    predecesor.getCablesVecinos().Remove(cableToPredecesor);
                    Emisor emisor = predecesor as Emisor;
                    if (emisor != null) {
                        emisor.repartirEnergía();
                    }
                    Destroy(cableToPredecesor.gameObject);
                }

                // Revisamos los sucesores de este nodo, desconectando su cable.
                // Recordamos que solo los Receptores pueden ser sucesores, por lo que
                // hacemos que vuelva a revisar su energía de entre sus Emisores.
                foreach (Conectable sucesor in this.sucesores) {
                    Cable cableToSucesor = getCableToVecino(sucesor);
                    sucesor.getPredecesores().Remove(this);
                    sucesor.getCablesVecinos().Remove(cableToSucesor);
                    Receptor receptor = sucesor as Receptor;
                    if (receptor != null) {
                        receptor.revisarEnergía();
                    }
                    Destroy(cableToSucesor.gameObject);
                }
                // Ya que desconectamos todos sus vecinos, destruimos este nodo
                Destroy(gameObject);
            }
        }
    }


    //void OnMouseUp() {
    //    if (clickEnable) {
    //        clickEnable = false;
    //        StartCoroutine(trapDoubleClicks(DoubleClickTimeout));
    //    }

    //}


    //IEnumerator trapDoubleClicks(float timer) {
    //    Debug.Log("Starting to listen for double clicks");
    //    float endTime = Time.time + timer;
    //    while (Time.time < endTime) {
    //        if (Input.GetMouseButtonDown(0)) {
    //            Debug.Log("Double click!");
    //            foreach(Conectable vecino in this.getVecinos()) {
    //                desconectar(this, vecino);
    //            }
    //            Destroy(gameObject);
    //            yield return new WaitForSeconds(0.4f);
    //            clickEnable = true;
    //            doubleClick = true;

    //        }
    //        yield return 0;
    //    }

    //    if (!doubleClick) {
    //        Debug.Log("Single click");

    //    }
    //    else {
    //        doubleClick = false;
    //    }

    //    clickEnable = true;
    //    yield return 0;
    //}

}