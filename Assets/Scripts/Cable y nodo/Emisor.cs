using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Emisor {

    public abstract int energíaPorConexión();

    public abstract void repartirEnergía();
}
