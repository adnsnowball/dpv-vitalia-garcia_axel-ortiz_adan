using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase que modela el comportamiento de los nodos.
 * </summary>
 */
public class Nodo : Conectable, Emisor, Receptor {
    /**
     * La cantidad total de energía que está recibiendo en tiempo real el nodo. 
     */
    [SerializeField]
    int energíaNodo;

    /**
     * Precio del nodo.
     */
    [SerializeField]
    int precioNodo;

    /**
     * bool que indica si este Nodo está energizado
     */
    [SerializeField]
    bool energizado;

    /**
     * El Animator de este Nodo
     */
    Animator animator;

    /**
     * <summary>
     * Getter de energíaNodo.
     * </summary>
     * <returns>
     * this.energíaNodo
     * </returns>
     */
    public int getEnergíaNodo()
    {
        return this.energíaNodo;
    }

    public int getPrecioNodo() {
        return this.precioNodo;
    }

    public void setEnergíaNodo(int energíaNodo) {
        this.energíaNodo = energíaNodo;
    }

    public void setPrecioNodo(int precioNodo) {
        this.precioNodo = precioNodo;
    }

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        animator.SetBool("Energizado", energizado);
    }

    public int energíaPorConexión() {
        int energíaPorConexion = energíaNodo;
        int numSucesores = this.getSucesores().Count;
        if (numSucesores > 0) {
            energíaPorConexion = energíaNodo / numSucesores;
        }
        return energíaPorConexion;
    }

    public void repartirEnergía() {
        foreach (Conectable sucesor in this.getSucesores()) {
            Receptor receptor = sucesor as Receptor;
            if (receptor != null) {
                receptor.revisarEnergía();
            }
        }
    }

    public void revisarEnergía() {
        this.energíaNodo = 0;
        foreach (Conectable predecesor in this.getPredecesores()) {
            Emisor emisor = predecesor as Emisor;
            if (emisor != null) {
                this.energíaNodo += emisor.energíaPorConexión();
            }
        }
        if(this.energíaNodo > 0) {
            energizado = true;
        }
        else {
            energizado = false;
        }
        repartirEnergía();
    }

}
