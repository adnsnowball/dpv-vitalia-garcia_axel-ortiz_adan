using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase Cable que contiene funciones, par�metros sobre el objeto cable.
 * </summary>
 */
public class Cable : MonoBehaviour
{

    /**
     * <summary>
     * La cantidad de energ�a que transporta el cable, s�lamente transporta de una sola fuente a la vez.
     * </summary>
     */
    [SerializeField]
    private int energ�a;

    /**
     * <summary>
     * Booleano que indica si est� pasando energ�a o no.
     * </summary>
     */
    [SerializeField]
    private bool transmitiendo;

    /**
     * <summary>
     * Booleanos que indican si el cable tiene sus terminaciones en conexi�n, es decir, si est� conectado de ambos extremos.
     * Si estos dos par�metros son true, el par�metro transmitiendo podr�a activarse.
     * Conectado Extremo Derecho / Izquierdo
     * </summary>
     */
    [SerializeField]
    private Conectable predecesor, sucesor;

    /**
     * Animator de este Cable.
     */
    Animator animator;

    /**
     * <summary>
     * Getter de energ�a.
     * </summary>
     * <returns>
     * this.energ�a
     * </returns>
     */
    public int getEnerg�a()
    {
        return this.energ�a;
    }


    /**
     * <summary>
     * Getter de transmitiendo.
     * </summary>
     * <returns>
     * this.transmitiendo
     * </returns>
     */
    public bool getTransmitiendo()
    {
        return this.transmitiendo;
    }

    /**
     * <summary>
     * Getter de conectadoExtDer.
     * </summary>
     * <returns>
     * this.conectadoExtDer
     * </returns>
     */
    public Conectable getOutput()
    {
        return this.predecesor;
    }

    /**
     * <summary>
     * Getter de conectadoExtIzq.
     * </summary>
     * <returns>
     * this.conectadoExtIzq
     * </returns>
     */
    public Conectable getInput()
    {
        return this.sucesor;
    }

    /**
     * <summary>
     * Setter de transmitiendo.
     * </summary>
     * <param name="transmitiendo">
     * El nuevo estado de transmitiendo de este cable.
     * </param>
     */
    public void setTransmitiendo(bool transmitiendo)
    {
        this.transmitiendo = transmitiendo;
    }

    /**
     * <summary>
     * Setter de energ�a.
     * </summary>
     * <param name="energ�a">
     * La nueva energ�a de este cable.
     * </param>
     */
    public void setEnerg�a(int energ�a) {
        this.energ�a = energ�a;
    }

    public void setPredecesor(Conectable predecesor) {
        this.predecesor = predecesor;
    }

    public void setSucesor(Conectable sucesor) {
        this.sucesor = sucesor;
    }

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        animator.SetBool("Transmitiendo", transmitiendo);
    }

    public void conectar() {
        this.setTransmitiendo(true);
        if (predecesor.permiteConectar() && sucesor.permiteConectar()) {
            Emisor emisor = predecesor as Emisor;
            Receptor receptor = sucesor as Receptor;
            if (emisor != null && receptor != null) {
                predecesor.getSucesores().Add(sucesor);
                predecesor.getCablesVecinos().Add(this);
                sucesor.getPredecesores().Add(predecesor);
                sucesor.getCablesVecinos().Add(this);
                emisor.repartirEnerg�a();
            }
        }
    }

    public void desconectar() {
        this.setTransmitiendo(false);
        Emisor emisor = predecesor as Emisor;
        Receptor receptor = sucesor as Receptor;
        if (emisor != null && receptor != null) {
            predecesor.getSucesores().Remove(sucesor);
            sucesor.getPredecesores().Remove(predecesor);
            sucesor.getCablesVecinos().Remove(this);
            predecesor.getCablesVecinos().Remove(this);
            receptor.revisarEnerg�a();
            emisor.repartirEnerg�a();
        }
    }

    private void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            Debug.Log("Click derecho");
            desconectar();
            Destroy(gameObject);
        }
    }

}