using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Receptor {

    public abstract void revisarEnergía();

}
