using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntoExtra : MonoBehaviour {

    /**
     * bandera booleana que indica si ya se energiz� este punto extra.
     */
    [SerializeField]
    bool energizado;

    /**
     * Animator de este Punto Extra.
     */
    Animator animator;

    public bool isEnergizado() {
        return energizado;
    }

    public void setEnergizado(bool energizado) {
        this.energizado = energizado;
    }

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        animator.SetBool("Energizado", energizado);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Cable cable = collision.GetComponent<Cable>();
        if (cable) {
            Debug.Log("Hola. Este punto extra se energiz� :D");
            revisarEnergizado(cable);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
        Cable cable = collision.GetComponent<Cable>();
        if (cable) {
            revisarEnergizado(cable);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        Cable cable = collision.GetComponent<Cable>();
        if (cable) {
            Debug.Log("Hola. Este punto extra se ya no est� energizado D:");
            energizado = false;
        }
    }

    private void revisarEnergizado(Cable cable) {
        if (cable.getTransmitiendo()) {
            energizado = true;
        }
        else {
            energizado = false;
        }
    }

}
