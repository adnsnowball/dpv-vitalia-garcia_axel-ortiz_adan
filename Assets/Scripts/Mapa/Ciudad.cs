using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** 
 * <summary>
 * Clase que modela el comportamiento de las ciudades u objetivos en el juego.
 * </summary>
 */
public class Ciudad : Conectable, Receptor {

    /**
     * La cantidad de energía requerida para estar en funcionamiento (satisfacer el objetivo). 
     */
    [SerializeField]
    int energíaRequerida;


    /**
     * La cantidad de energía que tiene actualmente la ciudad.
     */
    [SerializeField]
    int energíaCiudad;


    /**
     * Variable que describe si la ciudad tiene la cantidad de energía requerida, es decir, si el objetivo para esta ciudad está cumplido. 
     */
    [SerializeField]
    bool energizada;

    /**
     * TextMesh con la informaciónd e la ciudad.
     */
    TextMesh label;

    Animator animator;

    /**
     * <summary>
     * Getter de energíaCiudad.
     * </summary>
     * <returns>
     * this.energíaCiudad
     * </returns>
     */
    public int getEnergíaCiudad() {
        return this.energíaCiudad;
    }

    /**
     * <summary>
     * Getter de energíaRequerida.
     * </summary>
     * <returns>
     * this.energíaRequerida
     * </returns>
     */
    public int getEnergíaRequerida()
    {
        return this.energíaRequerida;
    }


    /**
     * <summary>
     * Getter de energizadoCiudad.
     * </summary>
     * <returns>
     * this.energizadoCiudad
     * </returns>
     */
    public bool isEnergizada()
    {
        return this.energizada;
    }

    /**
     * <summary>
     * Setter de energíaRequerida
     * </summary>
     * <param name="energíaRequerida">
     * Cantidad de energía requerida para satisfacer objetivo.
     * </param>
     */
    public void setEnergíaRequerida(int energíaRequerida)
    {
        this.energíaRequerida = energíaRequerida;
    }


    /**
 * <summary>
 * Setter de energizadoCiudad.
 * </summary>
 * <param name="energizadoCiudad">
 * El nuevo estado de energizadoCiudad, determina si la ciudad está en completo funcionamiento, es decir, si la cantidad de energía que recibe es al menos, la cantidad de energía requerida.
 * </param>
 */
    public void setEnergizadoCiudad(bool energizadoCiudad)
    {
        this.energizada = energizadoCiudad;
    }


    /** 
     * <summary>
     * Constructor de Ciudad. Las ciudades empeizan sin ser energizadas.
     * </summary>
     */
    public Ciudad()
    {
        setEnergizadoCiudad(false);
    }

    private void Awake() {
        label = gameObject.GetComponentInChildren<TextMesh>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        label.text = energíaCiudad + "/" + energíaRequerida;
        animator.SetBool("Energizada", energizada);
    }

    public void revisarEnergía() {
        // Reiniciamos la energía de la ciudad, y volvemos a revisar la aportación
        // de cada Emisor
        this.energíaCiudad = 0;
        foreach (Conectable predecesor in this.getPredecesores()) {
            Emisor emisor = predecesor as Emisor;
            if (emisor != null) {
                this.energíaCiudad += emisor.energíaPorConexión();
            }
        }
        if (energíaCiudad >= energíaRequerida) {
            setEnergizadoCiudad(true);
            Debug.Log("¡Listo! Esta ciudad está energizada.");
        }
        else {
            setEnergizadoCiudad(false);
            Debug.Log("Esta ciudad aún no recibe la energía suficiente.");
        }
    }

}
